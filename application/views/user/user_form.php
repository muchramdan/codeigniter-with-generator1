<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">User <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Full Name <?php echo form_error('full_name') ?></label>
            <input type="text" class="form-control" name="full_name" id="full_name" placeholder="Full Name" value="<?php echo $full_name; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Username <?php echo form_error('username') ?></label>
            <input type="text" class="form-control" name="username" id="username" placeholder="Username" value="<?php echo $username; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Email <?php echo form_error('email') ?></label>
            <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $email; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Password Hash <?php echo form_error('password_hash') ?></label>
            <input type="text" class="form-control" name="password_hash" id="password_hash" placeholder="Password Hash" value="<?php echo $password_hash; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Auth Key <?php echo form_error('auth_key') ?></label>
            <input type="text" class="form-control" name="auth_key" id="auth_key" placeholder="Auth Key" value="<?php echo $auth_key; ?>" />
        </div>
	    <div class="form-group">
            <label for="smallint">Status <?php echo form_error('status') ?></label>
            <input type="text" class="form-control" name="status" id="status" placeholder="Status" value="<?php echo $status; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Password Reset Token <?php echo form_error('password_reset_token') ?></label>
            <input type="text" class="form-control" name="password_reset_token" id="password_reset_token" placeholder="Password Reset Token" value="<?php echo $password_reset_token; ?>" />
        </div>
	    <div class="form-group">
            <label for="enum">User Level <?php echo form_error('user_level') ?></label>
            <input type="text" class="form-control" name="user_level" id="user_level" placeholder="User Level" value="<?php echo $user_level; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Role <?php echo form_error('role') ?></label>
            <input type="text" class="form-control" name="role" id="role" placeholder="Role" value="<?php echo $role; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Created At <?php echo form_error('created_at') ?></label>
            <input type="text" class="form-control" name="created_at" id="created_at" placeholder="Created At" value="<?php echo $created_at; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Updated At <?php echo form_error('updated_at') ?></label>
            <input type="text" class="form-control" name="updated_at" id="updated_at" placeholder="Updated At" value="<?php echo $updated_at; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('user') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>