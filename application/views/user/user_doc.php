<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>User List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Full Name</th>
		<th>Username</th>
		<th>Email</th>
		<th>Password Hash</th>
		<th>Auth Key</th>
		<th>Status</th>
		<th>Password Reset Token</th>
		<th>User Level</th>
		<th>Role</th>
		<th>Created At</th>
		<th>Updated At</th>
		
            </tr><?php
            foreach ($user_data as $user)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $user->full_name ?></td>
		      <td><?php echo $user->username ?></td>
		      <td><?php echo $user->email ?></td>
		      <td><?php echo $user->password_hash ?></td>
		      <td><?php echo $user->auth_key ?></td>
		      <td><?php echo $user->status ?></td>
		      <td><?php echo $user->password_reset_token ?></td>
		      <td><?php echo $user->user_level ?></td>
		      <td><?php echo $user->role ?></td>
		      <td><?php echo $user->created_at ?></td>
		      <td><?php echo $user->updated_at ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>